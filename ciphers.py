import string

"""
The Playfair cipher is a multiple letter encryption cipher that uses a substitution technique.
"""


def text_divider(txt):
    """
        divide the into list of pairs and add Z if its odd length
    """
    # append Z if Two letters are being repeated
    for s in range(0, len(txt), 2):
        if s < len(txt)-1:
            if txt[s] == txt[s+1]:
                txt = txt[:s+1]+"Z"+txt[s+1:]
    # append X if the total letters are odd, to make plaintext even
    if len(txt) % 2 != 0:
        txt = txt + "Z"

    out = [txt[i:i + 2] for i in range(0, len(txt), 2)]
    return out


def playfair_cipher(key):
    """
        create a list(table) from the english alphabet and the key entered
    """
    alpha = string.ascii_uppercase
    # make a playfair matrix contains the key as initial value
    playfair_matrix = []

    # remove redunduncy and spaces
    playfair_matrix.extend(
        ''.join(dict.fromkeys(key.upper().replace(" ", ""))))

    for l in alpha:
        if l not in playfair_matrix:
            if l == "I":
                playfair_matrix.append("J")
            else:
                playfair_matrix.append(l)
    return playfair_matrix


def playfair_encryption(txt, pf_list):
    """
        Encrypt an entered text using playfair matrix constructed, and
        retrun the encrypted text  
    """
    text_list = text_divider(txt.upper().replace(" ", ""))
    out_list = []

    # for letter in text_list:
    for i in range(len(text_list)):
        pair = text_list[i]
        if 'I' in pair:
            pair = pair.replace("I", "J")

        if pair[0] and pair[1] in pf_list:

            for i in range(2):
                if pf_list.index(pair[i]) % 5 and pf_list.index(pair[i]) != 1:
                    out_list.append(pf_list[pf_list.index(pair[i])-4])
                else:
                    out_list.append(pf_list[pf_list.index(pair[i])+1])

    return "".join(out_list)


def display_pf_table(pf_c):
    """
        display playfair table
    """
    s = "|\n".join(["|".join(pf_c[i:i+5]) for i in range(0, len(pf_c), 5)])
    return "playfair table({}):\n{}|".format(len(pf_c), s)


def main():
    plainText = input("Enter plaintext: ")
    key = input("Enter key: ")
    pf_c = playfair_cipher(key)
    pf_table = display_pf_table(pf_c)
    output = playfair_encryption(plainText, pf_c)
    print(pf_table)
    print("plaintext: {}\nplayfair_Encrypti0n: {}".format(plainText, output))


if __name__ == "__main__":
    main()
